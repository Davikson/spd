﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Algorytm4_BnB
{
    class BnB
    {
        static void Main(string[] args)
        {
            Console.Title = "Algorytm4 - BnB";
            string[] ReadText;
            int n = 0;
            List<Task> Tasks = new List<Task>();

            ReadText = File.ReadAllLines(args[0]);

            n = int.Parse(ReadText[0]);
            for(int i = 1; i <= n; i++)
            {
                int[] pwd = ReadText[i].Split(' ').Select(x => Convert.ToInt32(x)).ToArray();
                Tasks.Add(new Task(pwd[0], pwd[1], pwd[2]));
            }
            Node[] permutations = new Node[1 << n];
            for (int i = 1; i < permutations.Count(); i++)
            {
                if ((i & (i - 1)) == 0)
                {
                    int index = (int)(Math.Log10(i)/Math.Log10(2));
                    int c = (Tasks[index].p - Tasks[index].d) * Tasks[index].w;
                    permutations[i] = new Node(Tasks[index].p, (c > 0) ? c : 0);
                }
                else
                {
                    List<int> c = new List<int>();
                    int p = 0;
                    for (int j = 1; j < permutations.Count(); j <<= 1)
                    {
                        if ((i & j) != 0)
                        {
                            int index = (int)(Math.Log10(j) / Math.Log10(2));
                            int index2 = (int)(Math.Log10(i - j) / Math.Log10(2));
                            p += Tasks[index].p;
                            int c2_ = (permutations[i - j].p + Tasks[index].p - Tasks[index].d) * Tasks[index].w;
                            c.Add(permutations[i - j].c + ((c2_ > 0) ? c2_ : 0));
                        }
                    }
                    permutations[i] = new Node(p ,c.Min() > 0 ? c.Min() : 0);
                }
            }
            File.WriteAllText(args[1], permutations.Last().c.ToString());
            Console.WriteLine(permutations.Last().c.ToString());
        }
    }
    class Node
    {
        public readonly int p;
        public readonly int c;
        public Node(int _p, int _c)
        {
            p = _p;
            c = _c;
        }
    }
    class Task
    {
        public readonly int p; // processing time
        public readonly int w; // weight
        public readonly int d; // deadline
        public Task(int _p, int _w, int _d)
        {
            p = _p;
            w = _w;
            d = _d;
        }
    }
}
