﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Algorytm5_NEH
{
    class NEH
    {
        static void Main(string[] args)
        {
            Console.Title = "Alorytm5 - NEH";
            string[] ReadText;
            int n = 0;
            List<Task> Tasks = new List<Task>();

            if (!File.Exists(args[0]))
                File.WriteAllText(args[0], "3\n1 2\n2 3\n5 3\n");
            ReadText = File.ReadAllLines(args[0]);

            n = int.Parse(ReadText[0]);
            for (int i = 1; i < n + 1; i++)
            {
                List<int> p = ReadText[i].Split(' ').Where(x => (int.TryParse(x, out int tmp)) == true).Select(x => Convert.ToInt32(x)).ToList();
                Tasks.Add(new Task(p));
            }

            int cmax = -1;
            Tasks = Tasks.OrderBy(x => -x.p.Sum()).ToList();
            List<Task> Processed = new List<Task> { Tasks.First() };
            Tasks.Remove(Tasks.First());
            while (Tasks.Count > 0)
            {
                Task tmp = Tasks.First();
                KeyValuePair<int, int> bestcmaxsofar = new KeyValuePair<int, int>(-1, Int32.MaxValue);
                for(int i = 0; i <= Processed.Count; i++)
                {
                    List<Task> tmpList = new List<Task>(Processed);
                    tmpList.Insert(i, tmp);
                    int tmpCmax = CalculateCmax(tmpList);
                    if (tmpCmax < bestcmaxsofar.Value)
                        bestcmaxsofar = new KeyValuePair<int, int>(i, tmpCmax);
                }
                Processed.Insert(bestcmaxsofar.Key, tmp);
                Tasks.Remove(tmp);
                cmax = bestcmaxsofar.Value;
            }

            File.WriteAllText(args[1], cmax.ToString());
            Console.WriteLine(cmax.ToString());
        }
        static int CalculateCmax(List<Task> Tasks)
        {
            int[] cmax = new int[Tasks[0].p.Count];
            foreach(Task t in Tasks)
            {
                cmax[0] += t.p[0];
                for(int i = 1; i < cmax.Count(); i++)
                {
                    cmax[i] = Math.Max(cmax[i - 1], cmax[i]) + t.p[i];
                }
            }

            return cmax.Last();
        }
    }
    struct Task
    {
        public readonly List<int> p;
        public Task(List<int> _p)
        {
            p = new List<int>(_p);
        }
    }
}
