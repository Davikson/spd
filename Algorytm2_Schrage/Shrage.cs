﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Algorytm2_Schrage
{
    class Shrage
    {
        static void Main(string[] args)
        {
            Console.Title = "Alorytm2 - Shrage";
            string[] ReadText;
            int n = 0;
            List<Task> Tasks = new List<Task>();
            List<Task> Ready = new List<Task>();
            Task processed;
            Task tmp;

            if (!File.Exists(args[0]))
                File.WriteAllText(args[0], "3\n1 2 1\n2 3 1\n5 3 2\n");
            ReadText = File.ReadAllLines(args[0]);

            n = int.Parse(ReadText[0]);
            for (int i = 1; i < n + 1; i++)
            {
                int[] rpq = ReadText[i].Split(' ').Select(x => Convert.ToInt32(x)).ToArray();
                Tasks.Add(new Task(rpq[0], rpq[1], rpq[2]));
            }

            int time = 0;
            int cmax = 0;
            processed = new Task(0, 0, int.MaxValue);
            Tasks = Tasks.OrderBy(task => task.r).ToList();
            processed = Tasks.First();
            while (Tasks.Count > 0 || Ready.Count > 0)
            {
                while(Tasks.Count > 0 && Tasks.First().r <= time)
                {
                    tmp = Tasks.First();
                    Ready.Add(tmp);
                    Tasks.Remove(tmp);
                    if (tmp.q > processed.q)
                    {
                        processed.p = time - tmp.r;
                        time = tmp.r;
                        if (processed.p > 0)
                            Ready.Add(processed);
                    }
                }
                if (Ready.Count == 0)
                    time = Tasks.First().r;
                else
                {
                    tmp = Ready.OrderBy(task => -task.q).First();
                    Ready.Remove(tmp);
                    processed = tmp;
                    time += tmp.p;
                    cmax = Math.Max(cmax, time + tmp.q);
                }
            }

            File.WriteAllText(args[1], cmax.ToString());
            Console.WriteLine(cmax.ToString());
        }
    }
    class Task
    {
        public readonly int r;
        public int p;
        public readonly int q;
        public Task(int _r, int _p, int _q)
        {
            r = _r;
            p = _p;
            q = _q;
        }

    };
}
