using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Algorytm3_Carlier
{
    class Carlier
    {
        static void Main(string[] args)
        {
            Console.Title = "Algorytm3 - Carlier";
            string[] ReadText;
            int n = 0;
            List<Task> Tasks = new List<Task>();
            int UB = int.MaxValue;
            
            ReadText = File.ReadAllLines(args[0]);

            n = int.Parse(ReadText[0]);
            for (int i = 1; i < n + 1; i++)
            {
                int[] rpq = ReadText[i].Split(' ').Select(x => Convert.ToInt32(x)).ToArray();
                Tasks.Add(new Task(rpq[0], rpq[1], rpq[2]));
            }

            CarlierAlgorithm(Tasks, ref UB);

            File.WriteAllText(args[1], UB.ToString());
            Console.WriteLine(UB.ToString());
        }
        static void CarlierAlgorithm(List<Task> Tasks, ref int UB)
        {
            List<KeyValuePair<int, Task>> permutation, optimalPermutation;
            Task a, b, c;
            int r, q, p;
            int LB = int.MaxValue;
            int U = Shrage.ShrageAlgorithm(Tasks, out permutation);

            if (U < UB)
            {
                UB = U;
                optimalPermutation = permutation;
            }
            b = permutation.Where(x => U == x.Key + x.Value.q).OrderBy(x => x.Key).Last().Value;
            a = permutation.GetRange(0, permutation.IndexOf(permutation.Find(x => x.Value == b)) + 1).Where(x => U == x.Value.r + b.q + permutation.GetRange(permutation.IndexOf(x), permutation.IndexOf(permutation.Find(y => y.Value == b)) - permutation.IndexOf(x) + 1).Sum(z => z.Value.p)).OrderBy(x => x.Key).First().Value;
            c = permutation.GetRange(permutation.IndexOf(permutation.Find(x => x.Value == a)), permutation.IndexOf(permutation.Find(x => x.Value == b)) - permutation.IndexOf(permutation.Find(x => x.Value == a)) + 1).Where(x => x.Value.q < b.q).OrderBy(x => x.Key).LastOrDefault().Value;
            if (c == null) return;
            List<KeyValuePair<int, Task>> tmpList = permutation.GetRange(permutation.IndexOf(permutation.Find(x => x.Value == c)) + 1, permutation.IndexOf(permutation.Find(x => x.Value == b)) - permutation.IndexOf(permutation.Find(x => x.Value == c)));
            r = tmpList.Min(x => x.Value.r);
            q = tmpList.Min(x => x.Value.q);
            p = tmpList.Sum(x => x.Value.p);
            c.r = Math.Max(c.r, r + p);
            LB = Shrage.ShragePrze(Tasks);
            if (LB < UB) CarlierAlgorithm(Tasks, ref UB);
            c.r = c.r2;
            c.q = Math.Max(c.q, q + p);
            LB = Shrage.ShragePrze(Tasks);
            if (LB < UB) CarlierAlgorithm(Tasks, ref UB);
            c.q = c.q2;
        }
    }
    static class Shrage
    {
        public static int ShrageAlgorithm(List<Task> Tasks, out List<KeyValuePair<int, Task>> permutation)
        {
            Task tmp;
            List<Task> Ready = new List<Task>();
            permutation = new List<KeyValuePair<int, Task>>();
            int time = 0;
            int cmax = 0;

            Tasks = Tasks.OrderBy(task => task.r).ToList();
            while (Tasks.Count > 0 || Ready.Count > 0)
            {
                while (Tasks.Count > 0 && Tasks.Min(x => x.r) <= time)
                {
                    tmp = Tasks.First();
                    Ready.Add(tmp);
                    Tasks.Remove(tmp);
                }
                if (Ready.Count == 0)
                    time = Tasks.First().r;
                else
                {
                    tmp = Ready.OrderBy(task => -task.q).First();
                    time += tmp.p;
                    Ready.Remove(tmp);
                    permutation.Add(new KeyValuePair<int, Task>(time, tmp));
                    cmax = Math.Max(cmax, time + tmp.q);
                }
            }
            return cmax;
        }
        public static int ShragePrze(List<Task> Tasks)
        {
            List<Task> copy = Tasks;
            Task processed = new Task(0, 0, int.MaxValue);
            Task tmp;
            List<Task> Ready = new List<Task>();
            int time = 0;
            int cmax = 0;

            Tasks = Tasks.OrderBy(task => task.r).ToList();
            processed = Tasks.First();
            while (Tasks.Count > 0 || Ready.Count > 0)
            {
                while (Tasks.Count > 0 && Tasks.First().r <= time)
                {
                    tmp = Tasks.First();
                    Ready.Add(tmp);
                    Tasks.Remove(tmp);
                    if (tmp.q > processed.q)
                    {
                        processed.p = time - tmp.r;
                        time = tmp.r;
                        if (processed.p > 0)
                            Ready.Add(processed);
                    }
                }
                if (Ready.Count == 0)
                    time = Tasks.First().r;
                else
                {
                    tmp = Ready.OrderBy(task => -task.q).First();
                    Ready.Remove(tmp);
                    processed = tmp;
                    time += tmp.p;
                    cmax = Math.Max(cmax, time + tmp.q);
                }
            }
            foreach (Task task in copy) task.p = task.p2;
            return cmax;
        }
    }
    class Task
    {
        public int r;
        public int p;
        public int q;
        public readonly int r2;
        public readonly int p2;
        public readonly int q2;
        public Task(int _r, int _p, int _q)
        {
            r = _r;
            p = _p;
            q = _q;
            r2 = _r;
            p2 = _p;
            q2 = _q;
        }

    };
}
