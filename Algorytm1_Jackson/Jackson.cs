﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Algorytm1_Jackson
{
    class Jackson
    {
        static void Main(string[] args)
        {
            Console.Title = "Alorytm1 - Jackson";
            string[] ReadText;
            int n = 0;
            List<Task> Tasks = new List<Task>();

            if (!File.Exists(args[0]))
                File.WriteAllText(args[0], "3\n1 2\n2 3\n5 3\n");
            ReadText = File.ReadAllLines(args[0]);

            n = int.Parse(ReadText[0]);
            for (int i = 1; i < n + 1; i++)
            {
                int[] rp = ReadText[i].Split(' ').Select(x => Convert.ToInt32(x)).ToArray();
                Tasks.Add(new Task(rp[0], rp[1]));
            }

            Tasks = Tasks.OrderBy(task => task.r).ToList();
            int time = 0;
            while(Tasks.Count > 0)
            {
                if (Tasks[0].r <= time)
                {
                    time += Tasks[0].p;
                    Tasks.Remove(Tasks[0]);
                }
                else
                    time++;
            }

            File.WriteAllText(args[1], time.ToString());
            Console.WriteLine(time.ToString());
        }
        struct Task
        {
            public readonly int r;
            public readonly int p;
            public Task(int _r, int _p)
            {
                r = _r;
                p = _p;
            }

        };
    }
}
